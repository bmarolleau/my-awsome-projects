##  References 
- ibmcloud CLI

[https://cloud.ibm.com/docs/power-iaas-cli-plugin?topic=power-iaas-cli-plugin-power-iaas-cli-reference#ibmcloud-pi-key-create](https://cloud.ibm.com/docs/power-iaas-cli-plugin?topic=power-iaas-cli-plugin-power-iaas-cli-reference#ibmcloud-pi-key-create)

- ssh setup

[https://cloud.ibm.com/docs/infrastructure/power-iaas?topic=power-iaas-create-vm#ssh-setup](https://cloud.ibm.com/docs/infrastructure/power-iaas?topic=power-iaas-create-vm#ssh-setup)

- Power VS API

[https://cloud.ibm.com/apidocs/power-cloud#introduction](https://cloud.ibm.com/apidocs/power-cloud#introduction)

## SAVE / RESTORE AIX/IBM i - Migration to Cloud (January 2020)**

[https://ibm.ent.box.com/s/ncrx39ji94l3ra93ph25wc8gwx3sv898](https://ibm.ent.box.com/s/ncrx39ji94l3ra93ph25wc8gwx3sv898)

1)  VIA PowerVC (connected to the SAN Storage)

Export OVA - save the system + Data. then transfer the OVA (import)

2) IBM i:

SAVSYS (distribution media) > BRMS > ICC > COS …. then in Cloud: New IBM-i create > BRMS restore using ICC from COS

BRMS/ICC/COS alternative : FTP the images directly to the IBM i

3) AIX: mksysb then transfer to existing Power VS VM - alt_mksysb_install

if datavg, savevg / restvg. (same as mksysb)

4) MDM 

with method 2 to transfer the backup file(s). Restore from COS

### Tests for AIX 
ibmcloud pi instance 2005ad05-72a0-4d89-a8df-241a326fef69 --json

#### $ ibmcloud pi instances

Listing instances under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name**  **Path**

**c17d4380-f762-40ec-97f7-4a02915db7b6** keytest-vm /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/c17d4380-f762-40ec-97f7-4a02915db7b6

**2005ad05-72a0-4d89-a8df-241a326fef69** AIX1 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/2005ad05-72a0-4d89-a8df-241a326fef69

#### $ ibmcloud pi instance-create keytest-vm2 --image 7100-05-04 --memory 2 --networks 2feed78d-fc40-4a7f-8f79-9f19c2e86e1b --processors 1 --processor-type shared --key-name TEST

Creating instance **keytest-vm2** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

Instance **keytest-vm2** already exists, created on **0001-01-01T00:00:00.000Z**.

**ID** 8c229435-b9b9-4c10-aac1-0752601a3714

**Name** keytest-vm2

**CPU Cores** 1

**Memory** 2

**Processor Type** shared

**Networks** 2feed78d-fc40-4a7f-8f79-9f19c2e86e1b

**Disk Size** 20

**Volumes** -

**Image** d8b5dff9-973b-4107-83d5-a705076ff6f0

**Created** 0001-01-01T00:00:00.000Z

**Last Updated** 0001-01-01T00:00:00.000Z

**Status** BUILDING

**Progress** 0

**Address** Internal Address: , Mac Address:

#### $ ibmcloud pi instances

Listing instances under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name** **Path**

**8c229435-b9b9-4c10-aac1-0752601a3714** keytest-vm2 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/8c229435-b9b9-4c10-aac1-0752601a3714

**c17d4380-f762-40ec-97f7-4a02915db7b6** keytest-vm /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/c17d4380-f762-40ec-97f7-4a02915db7b6

**2005ad05-72a0-4d89-a8df-241a326fef69** AIX1 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/2005ad05-72a0-4d89-a8df-241a326fef69

#### $ ibmcloud pi instance-delete c17d4380-f762-40ec-97f7-4a02915db7b6

Deleting instance **c17d4380-f762-40ec-97f7-4a02915db7b6** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**OK**

Instance c17d4380-f762-40ec-97f7-4a02915db7b6 is deleted.

#### Test connectivity with $ ssh -i ./id_rsa [root@161.156.139.83](mailto:root@161.156.139.83)

1 unsuccessful login attempt since last login.

Last unsuccessful login: Fri Jan 10 09:03:38 2020 on ssh from 195.212.29.167

*******************************************************************************

* Welcome to AIX Version 7.1! *

* Please see the README file in /usr/lpp/bos for information pertinent to *

* this release of the AIX Operating System. *

*******************************************************************************

#

#### $ ibmcloud pi networks

Listing networks under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name** **Address**

**2feed78d-fc40-4a7f-8f79-9f19c2e86e1b** public-192_168_130_224-29-VLAN_2024 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/networks/2feed78d-fc40-4a7f-8f79-9f19c2e86e1b

AIX1: private IP: 192.168.130.227

AIX test: 192.168.130.226

same network

"networks": [

{

"externalIP": "161.156.139.82",

"href": "/pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/8c229435-b9b9-4c10-aac1-0752601a3714/networks/2feed78d-fc40-4a7f-8f79-9f19c2e86e1b",

"ip": "192.168.130.226",

"ipAddress": "192.168.130.226",

"macAddress": "fa:3e:bd:ec:c4:20",

"networkID": "2feed78d-fc40-4a7f-8f79-9f19c2e86e1b",

"networkName": "public-192_168_130_224-29-VLAN_2024",

"type": "fixed",

"version": 4

}

####  $ ibmcloud pi instance-get-console 8c229435-b9b9-4c10-aac1-0752601a3714

Getting console for instance **8c229435-b9b9-4c10-aac1-0752601a3714** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**Name** 8c229435-b9b9-4c10-aac1-0752601a3714

**Console URL** [https://fra04-console.power-iaas.cloud.ibm.com/console/index.html?token=e0646686-be42-4dc0-bf61-429c1e8b5e2e](https://fra04-console.power-iaas.cloud.ibm.com/console/index.html?token=e0646686-be42-4dc0-bf61-429c1e8b5e2e)

![](https://mail.notes.na.collabserv.com/data0/126/20913429.nsf/($Inbox)/2fba54f1b11f76ce002585660054c59a/Body/M2?OpenElement&cid=766193938469&OpenSoftDeleted)

#### $ ibmcloud pi images

Listing images under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name**  **Address**

**d8b5dff9-973b-4107-83d5-a705076ff6f0** 7100-05-04 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/images/d8b5dff9-973b-4107-83d5-a705076ff6f0

#### $ ibmcloud pi instance-list-volumes 2005ad05-72a0-4d89-a8df-241a326fef69

Listing volumes attached to instance **2005ad05-72a0-4d89-a8df-241a326fef69** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name** **Address**

**80ca6d9c-f02d-417e-af43-e80735922eb9** AIX1-2005ad05-000005e6-boot-0 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/volumes/80ca6d9c-f02d-417e-af43-e80735922eb9

**8435aee5-ac1d-48c0-a160-856554e10600** disk1 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/volumes/8435aee5-ac1d-48c0-a160-856554e10600

# lspv

hdisk0 none None

hdisk1 00f6db0af58e9775 rootvg active

air-de-benoit:PowerVSdemo Benoit2$ ibmcloud pi volume-delete 8435aee5-ac1d-48c0-a160-856554e10600

Deleting volume **8435aee5-ac1d-48c0-a160-856554e10600** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**OK**

Volume ID 8435aee5-ac1d-48c0-a160-856554e10600 is deleted.

bash-4.3# lsdev -Cc disk

hdisk0 Available C4-T1-01 MPIO IBM 2076 FC Disk

hdisk1 Available C4-T1-01 MPIO IBM 2076 FC Disk

ash-4.3# rmdev -dRl hdisk0

hdisk0 deleted

bash-4.3# lspv

hdisk1 00f6db0af58e9775 rootvg active

bash-4.3# cfgmgr

bash-4.3# lspv

hdisk1 00f6db0af58e9775 rootvg active

![](https://mail.notes.na.collabserv.com/data0/126/20913429.nsf/($Inbox)/2fba54f1b11f76ce002585660054c59a/Body/M3?OpenElement&cid=68291880980&OpenSoftDeleted)

![](https://mail.notes.na.collabserv.com/data0/126/20913429.nsf/($Inbox)/2fba54f1b11f76ce002585660054c59a/Body/M4?OpenElement&cid=53360522294&OpenSoftDeleted)

#### $ ibmcloud pi network-create-private internal --cidr-block 10.0.44.0/24 --ip-range "10.0.44.1-10.0.44.99,10.0.44.101-10.0.44.253" --dns-servers "127.0.0.1" --gateway 10.0.44.254

#### $ ibmcloud pi network-create-private internal --cidr-block 10.0.44.0/24 --ip-range "10.0.44.2-10.0.44.253" --dns-servers "127.0.0.1" --gateway 10.0.44.254

Creating network **internal** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

Network **internal** created.

**ID** d78d0432-48b8-4766-8618-dd43c6145086

**Name** internal

**Type** vlan

**VLAN** 246

**CIDR Block** 10.0.44.0/24

**IP Range** [10.0.44.2 10.0.44.253 ]

**Gateway** 10.0.44.254

**DNS** 127.0.0.1

####  $ ibmcloud pi instance-create aix1 --image 7100-05-04 --memory 2 --networks 15c2d195-e241-4132-ae9f-823e4e278089  --processors 1 --processor-type shared --key-name TEST

####  $ ibmcloud pi instance-create aix2 --image 7100-05-04 --memory 2 --networks 15c2d195-e241-4132-ae9f-823e4e278089  --processors 1 --processor-type shared --key-name TEST

#### $ ibmcloud pi instances

Listing instances under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**ID**  **Name**  **Path**

**5812a221-bdca-4d17-9fbc-8c620d6c0d83** aix2 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/5812a221-bdca-4d17-9fbc-8c620d6c0d83

**3412d671-24a1-4e6e-aeb7-3df253ff79df** aix1 /pcloud/v1/cloud-instances/ca78784c50764f94b96808fd9543c1e2/pvm-instances/3412d671-24a1-4e6e-aeb7-3df253ff79df

air-de-benoit:PowerVSdemo Benoit2$ ibmcloud pi instance 3412d671-24a1-4e6e-aeb7-3df253ff79df | grep Status

Status ACTIVE

Health Status PENDING

air-de-benoit:PowerVSdemo Benoit2$ ibmcloud pi instance-get-console 3412d671-24a1-4e6e-aeb7-3df253ff79df

Getting console for instance **3412d671-24a1-4e6e-aeb7-3df253ff79df** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**Name** 3412d671-24a1-4e6e-aeb7-3df253ff79df

**Console URL** [https://fra04-console.power-iaas.cloud.ibm.com/console/index.html?token=e85f4ca4-29e0-4b94-8633-996f4becae3a](https://fra04-console.power-iaas.cloud.ibm.com/console/index.html?token=e85f4ca4-29e0-4b94-8633-996f4becae3a)

![](https://mail.notes.na.collabserv.com/data0/126/20913429.nsf/($Inbox)/2fba54f1b11f76ce002585660054c59a/Body/M5?OpenElement&cid=234785185802&OpenSoftDeleted)

#### $ ibmcloud pi instance-delete 5812a221-bdca-4d17-9fbc-8c620d6c0d83

Deleting instance **5812a221-bdca-4d17-9fbc-8c620d6c0d83** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**OK**

Instance 5812a221-bdca-4d17-9fbc-8c620d6c0d83 is deleted.

air-de-benoit:PowerVSdemo Benoit2$ ibmcloud pi instance-delete 3412d671-24a1-4e6e-aeb7-3df253ff79df

Deleting instance **3412d671-24a1-4e6e-aeb7-3df253ff79df** under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)**...

**OK**

Instance 3412d671-24a1-4e6e-aeb7-3df253ff79df is deleted.

![](https://mail.notes.na.collabserv.com/data0/126/20913429.nsf/($Inbox)/2fba54f1b11f76ce002585660054c59a/Body/M6?OpenElement&cid=936799114929&OpenSoftDeleted)

#### $ ibmcloud target -r us-south

#### $ ibmcloud pi service-list

Listing services under account **BENOIT MAROLLEAU's Account** as user **[benoit.marolleau@fr.ibm.com](mailto:benoit.marolleau@fr.ibm.com)** in region **us-south**...

**ID** **Name**

**crn:v1:bluemix:public:power-iaas:us-south:a/45d319a4b25971b53b40137d095ab213:f0f71cbe-7997-4924-8321-7f876a494973::** Power Systems Virtual Server-Service-MOP-USA-1

air-de-benoit:PowerVSdemo Benoit2$ ibmcloud pi service-target crn:v1:bluemix:public:power-iaas:us-south:a/45d319a4b25971b53b40137d095ab213:f0f71cbe-7997-4924-8321-7f876a494973::

Targeting service crn:v1:bluemix:public:power-iaas:us-south:a/45d319a4b25971b53b40137d095ab213:f0f71cbe-7997-4924-8321-7f876a494973::...
